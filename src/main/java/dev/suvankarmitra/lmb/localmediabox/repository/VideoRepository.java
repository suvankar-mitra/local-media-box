package dev.suvankarmitra.lmb.localmediabox.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import dev.suvankarmitra.lmb.localmediabox.model.VideoMeta;

@Repository
public interface VideoRepository extends JpaRepository<VideoMeta, Integer> {
    @SuppressWarnings("null")
    @Query("SELECT v FROM VideoMeta v WHERE v.videoMetaId = ?1")
    VideoMeta getById(Integer id);
}
