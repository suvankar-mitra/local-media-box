package dev.suvankarmitra.lmb.localmediabox.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class VideoMetaDto {
    private Integer id;
    private String videoTitle;
    private String localMediaPath;
    private String mimeType;
}
