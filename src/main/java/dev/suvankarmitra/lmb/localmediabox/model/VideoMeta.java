package dev.suvankarmitra.lmb.localmediabox.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class VideoMeta {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer videoMetaId;
    @Column(length = 1000)
    private String videoTitle;
    @Column(length = 1000)
    private String localMediaPath;
    private String mimeType;
}
