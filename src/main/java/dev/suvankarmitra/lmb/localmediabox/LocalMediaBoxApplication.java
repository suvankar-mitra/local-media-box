package dev.suvankarmitra.lmb.localmediabox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
public class LocalMediaBoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(LocalMediaBoxApplication.class, args);
	}

}
