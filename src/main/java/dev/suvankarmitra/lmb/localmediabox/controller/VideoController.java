package dev.suvankarmitra.lmb.localmediabox.controller;

import dev.suvankarmitra.lmb.localmediabox.dto.VideoMetaDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dev.suvankarmitra.lmb.localmediabox.model.VideoMeta;
import dev.suvankarmitra.lmb.localmediabox.service.VideoService;

import java.util.List;

@RestController
@RequestMapping("/lmb")
public class VideoController {

    private final VideoService videoService;

    public VideoController(VideoService videoService) {
        this.videoService = videoService;
    }

    @GetMapping("/{id}")
    public VideoMetaDto getVideoById(@PathVariable int id) {
        return videoMetaToDto(videoService.getVideoById(id));
    }

    @GetMapping("/all")
    public List<VideoMetaDto> getAllVideoDetails() {
        return videoService.getAllVideoDetails().stream().map(this::videoMetaToDto).toList();
    }

    @GetMapping("/sync/{path}")
    @ResponseBody
    public StatusDto syncAllVideos(@PathVariable String path) {
        videoService.syncAndSaveAll(path);
        return new StatusDto("Syncing...");
    }

    private VideoMetaDto videoMetaToDto(VideoMeta videoMeta) {
        VideoMetaDto videoMetaDto = new VideoMetaDto();
        videoMetaDto.setVideoTitle(videoMeta.getVideoTitle());
        videoMetaDto.setId(videoMeta.getVideoMetaId());
        videoMetaDto.setLocalMediaPath(videoMeta.getLocalMediaPath());
        videoMetaDto.setMimeType(videoMeta.getMimeType());
        return videoMetaDto;
    }

    public record StatusDto(String status) {
    }

}
