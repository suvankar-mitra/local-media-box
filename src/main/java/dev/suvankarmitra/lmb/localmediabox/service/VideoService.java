package dev.suvankarmitra.lmb.localmediabox.service;

import org.apache.tika.Tika;
import org.springframework.stereotype.Service;

import dev.suvankarmitra.lmb.localmediabox.model.VideoMeta;
import dev.suvankarmitra.lmb.localmediabox.repository.VideoRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class VideoService {

    private final VideoRepository repository;

    public VideoService(VideoRepository repository) {
        this.repository = repository;
    }

    public VideoMeta getVideoById(int id) {
        return repository.getById(id);
    }

    public List<VideoMeta> getAllVideoDetails() {
        return repository.findAll();
    }

    @SuppressWarnings("null")
    public void syncAndSaveAll(String root) {
        new Thread() {
            public void run() {
                Set<File> allFiles = listFilesUsingJavaIO(root);
                repository.saveAll(allFiles.stream()
                        .filter(file -> {
                            Tika tika = new Tika();
                            String mimeType = null;
                            try {
                                mimeType = tika.detect(file);
                            } catch (IOException ignored) {
                                mimeType = null;
                            }
                            if (mimeType.startsWith("video"))
                                return true;
                            else
                                return false;
                        })
                        .map(file -> {
                            VideoMeta videoMeta = new VideoMeta();
                            Tika tika = new Tika();
                            String mimeType = null;
                            try {
                                mimeType = tika.detect(file);
                            } catch (IOException ignored) {
                                mimeType = null;
                            }
                            videoMeta.setMimeType(mimeType);
                            videoMeta.setVideoTitle(file.getName());
                            videoMeta.setLocalMediaPath(file.getAbsolutePath());
                            return videoMeta;
                        }).collect(Collectors.toSet()));
            }
        }.start();

    }

    public Set<File> listFilesUsingJavaIO(String dir) {
        try {
            return Files.find(Paths.get(dir),
                    Integer.MAX_VALUE,
                    (filePath, fileAttr) -> fileAttr.isRegularFile()).map(Path::toFile).collect(Collectors.toSet());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
