# Local Media Box

This is a hobby project to build a [Jellyfin](https://github.com/jellyfin/jellyfin) like application to manage and stream your local media.

- I am not using any code from Jellyfin, just borrowing the idea from them. ❤️
